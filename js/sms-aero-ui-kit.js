var price = 0;
var channel = 7;
var phones = null;

function delivery_cost(c){
    jQuery('.mailign-page .phone-adder .total-contacts').html(c);
    var message_length = 160;
    if (/[а-я]/i.test(jQuery('.mailign-page .sms-content').val()))
        message_length=70;
    var total_sms = Math.ceil(jQuery('.mailign-page .sms-content').val().length / message_length);
    var st = $('.mailign-page select[name="settings_channel"]').get(0);
    channel = jQuery(st[st.selectedIndex]).data('channel');

    var result_box = jQuery('.box-count-aerosms');

    phones = jQuery('.mailign-page .phone-adder textarea').val().split(/\n/i);
    phones = phones.join();
    phones = phones.replace(/\-|\+/g,"");
    getSmsrice(phones, total_sms);
    result_box.find('.count-aerosmswrapper').html(total_sms * c);
}
var getSmsrice = function(phones, total_sms) {
    var sum = 0;
    if(phones == 0){
        sum = 0;
        $('.operator-count').html('0');
    } else {
        if (phones != '') {
            $('.operator-count').html('0');
            jQuery.ajax({
                url: 'https://gate.smsaero.ru/getSendingPrice',
                data: {'phones': phones, 'type': 'id'},
                dataType: 'json',
                method: 'POST',
                success: function (xhr) {
                    $.each(xhr.response, function (i, el) {
                        sum += price.response[channel][i] * el;
                        jQuery('.box-count-aerosms').find('.cost-aerosmswrapper').html((total_sms * sum).toFixed(2));
                        $('#operator-id-'+i).text(el*total_sms);
                    });
                }
            });
        }
    }
    return sum;
};
function show_notice(content, type){	type = (type == undefined) ? false : type;	document.getElementById('notice-side').innerHTML = '<div class="a-notice '+((type) ? 'a-success': 'a-fail')+'">'+content+'<i class="fa fa-close"></i></div>';	jQuery('#notice-side .fa-close').click(function(){		jQuery(this.parentNode).fadeOut(500, function(){			jQuery(this).remove();		});	});	setTimeout(function(){		jQuery('#notice-side .fa-close').click();	}, 5000);}
function recount_deliv_cost(is_req){
    var c = 0;
    if (jQuery('#to_who').val() != 1 || (is_req != undefined && jQuery('#to_who').val() == 1)){
        jQuery('.mailign-page .phone-adder textarea').val(jQuery('.mailign-page .phone-adder textarea').val().replace(/$[\s]*\n$/gmi,''));
        delivery_cost((jQuery('.mailign-page .phone-adder textarea').val() != '') ? jQuery('.mailign-page .phone-adder textarea').val().split(/\n/i).length : 0);
    } else {
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            dataType: 'json',
            data: { action: 'smsaero_get_phones', query: 'full', security:smsaero.nonce },
            success: function(numbers) {
                    var nt = '';
                    numbers.forEach(function(self){
                        nt += self + "\n";
                    });
                    nt = nt.slice(0, -1);
                    jQuery('.mailign-page .phone-adder textarea').val(nt);				
                    delivery_cost(jQuery('.mailign-page .phone-adder textarea').val().split(/\n/i).length);
            }
        })
    }
}

document.addEventListener('DOMContentLoaded', function (){
    jQuery.ajax({
        method: 'POST',
        url: '',
        dataType: 'json',
        data: '',
    });

    jQuery('.mailign-page .phone-adder textarea, .mailign-page .sms-content, .mailign-page select[name="settings_channel"]').change(function(){
        recount_deliv_cost(true);
    });


    jQuery('.order-statuses input[type="checkbox"]').each(function(){
        this.addEventListener('change', function(){
            if (this.checked){
                jQuery(this).parent().parent().find(' > *:not(:nth-child(2))').css('display', 'block');
            } else {
                jQuery(this).parent().parent().find(' > *:not(:nth-child(2))').css('display', 'none');
            }
        });
        if (this.checked){
            jQuery(this).parent().parent().find(' > *:not(:nth-child(2))').css('display', 'block');
        } else {
            jQuery(this).parent().parent().find(' > *:not(:nth-child(2))').css('display', 'none');
        }
    });




    jQuery('#immediate_send').click(function(){
        jQuery('.time_sending > div:not(:first-child)').css('display', 'none');
    });
    jQuery('#delay_send').click(function(){
        jQuery('.time_sending > div:not(:first-child)').css('display', 'block');
    });
    jQuery('#immediate_send').click();
    jQuery('.phone-adder button').click(function(){
        this.nextElementSibling.value += (/\n$|^$/.test(this.nextElementSibling.value) ? '' : '\n') + jQuery(this.previousElementSibling).find('input').val();
        $(this.previousElementSibling).find('input').val('');
        $(this.nextElementSibling).trigger('change');
    });
    jQuery('.phone-adder input').keypress(function(e){
        if (e.keyCode == 13)
            this.nextElementSibling.click();
    });
    jQuery('.phone-adder textarea').keypress(function(event){
        var key, keyChar;
        if(!event) var event = window.event;
        if (event.key == "Delete") return true;
        if (event.keyCode) key = event.keyCode;
        else if(event.which) key = event.which;

        if(key==null || key==0 || key==8 || key==13 || key==9 || key==37 || key==39 ) return true;
        keyChar=String.fromCharCode(key);

        if(!/\d|[\+]/.test(keyChar))	return false;
    });
    jQuery('.phone-adder textarea').keyup(function(){
        this.value = this.value.replace(/[^0-9\+\n]/i, '');
    });
    jQuery('.mailign-page .phone-adder textarea').change(function(){
        if (this.value != '') $(this).parent().find('.total-contacts').html(this.value.split(/\n/i).length);
    });
    jQuery('.mailign-page .phone-adder textarea').trigger('change');
    jQuery('#menu_switcher').click(function(){
        $(this).parent().toggleClass('open');
    });
    jQuery(".a-hider > ul > li").click(function(){
        jQuery(".a-hider > ul > li").removeClass('active');
        jQuery(this).addClass('active');
        jQuery(".notice-righ-side").css('display','none');
        jQuery("#"+jQuery(this).attr('data-tab')).css('display','block');
    });

    jQuery(".notice-righ-side .sms-content, .mailign-page .sms-content").on('keyup', function(){
        var info = jQuery(this).next();

        var message_length = 160;
        if (/[а-я]/i.test(this.value))
            message_length=70;
        var total_sms = Math.ceil(this.value.length / message_length);
        if (jQuery(this).hasClass('check-words')){
            var tmp = this.value.split(/\s/);
            var check = true;
            tmp.some(function(item){
                if (/[а-я]/i.test(item) && /[a-z]/i.test(item)){
                    jQuery(this).addClass('invalid');
                    info.addClass('invalid');
                    info.html('ВНИМАНИЕ! Ошибка в слове "'+item+'". Нельзя отправлять текст, где в слове одновременно используется символы латиницы и кириллицы');
                    check = false;
                    return item;
                }
            });
            if (check) {
                jQuery(this).removeClass('invalid');
                info.removeClass('invalid');
                info.html('<div class="input-info">Символов <span class="red-text"><span class="total-symbols"></span>/<span class="symbols-to-sms"></span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text"></span></div>');
            }
        }
        info.find('.total-symbols').html(this.value.length);
        info.find('.symbols-to-sms').html(message_length);
        info.find('.sms-count').html(total_sms);
    });
    jQuery(".notice-righ-side .sms-content, .mailign-page .sms-content").keyup();

    var o = jQuery(".js-date-sending");
    o.mask("99.99.9999", {placeholder: "_", autoclear: !1});
    o.each(function (o, e) {
        var i = jQuery(e);
        i.datepicker({
            onSelect: function (o) {
                setTimeout(function () {
                    i.val(o);
                }, 100);
            },
            minDate: new Date(),
            autoClose: true
        });
    });


    (function(){
        var els = document.querySelectorAll('.sms-aero .autocomplete');
        for (var i=0; i < els.length; i++){
            var list = document.createElement('div');
            list.setAttribute('class', 'a-items');
            list.style.display = 'none';
            list.innerHTML = '<ul></ul>';
            els[i].classList.add('a-selected-item');
            els[i].onclick = function(e){
                if (this.parentNode.classList.contains('opened')){
                    this.parentNode.classList.remove('opened');
                } else {
                    this.parentNode.classList.add('opened');
                    if (this.nextElementSibling.getElementsByTagName('ul').innerHTML == '') this.nextElementSibling.style.display = "none";
                }
            };

            els[i].addEventListener('keyup', function(){
                if(this.value == "") {
                    this.nextElementSibling.style.display = "none";
                    return true;
                }
                if (this.value.length < this.getAttribute('data-symbols-start') && this.hasAttribute('inputing')) return true;

                this.setAttribute('inputing', '');
                var self = this;
                setTimeout(function(){
                    self.removeAttribute('inputing');
                }, 500);

                jQuery.ajax({
                    url: ajaxurl,
                    type: 'post',
                    dataType: 'json',
                    data: { action: 'smsaero_get_phones', query: 'email', email: self.value, security:smsaero.nonce },
                    success: function(resp) {
                        self.nextElementSibling.getElementsByTagName('ul')[0].innerHTML = '';
                        if(jQuery.isEmptyObject(resp) == true) {
                            self.nextElementSibling.style.display = "none";
                        }
                        else {
                            self.nextElementSibling.style.display = "block";
                            for (var j = 0; j < resp.length; j ++){
                                var option = document.createElement('li');
                                option.innerHTML = resp[j];
                                option.onclick = function(){
                                    this.parentNode.parentNode.previousElementSibling.value = this.innerHTML;
                                    try {
                                        this.parentNode.parentNode.previousElementSibling.onchange();
                                    } catch(err){}
                                    try {
                                        jQuery(this.parentNode.parentNode.previousElementSibling).trigger('change');
                                    } catch(err){}
                                };
                            self.nextElementSibling.getElementsByTagName('ul')[0].appendChild(option);
                            }
                        }
                }
            })
        });

            els[i].parentNode.appendChild(list);
            els[i].parentNode.classList.add('a-select-wrapper');
        }



        var els = document.querySelectorAll('.sms-aero .time-select');
        for (var i=0; i < els.length; i++){
            jQuery(els[i]).mask("99:99", {placeholder: "_", autoclear: !1});
            var start = els[i].getAttribute('data-start');
            start = start.split(':');
            if (start.length != 2){
                console.log('--> time-select: wrong data-start attribute element: ', els[i]);
                continue;
            }
            start = Number(start[0] * 60) + Number(start[1]);
            var end = els[i].getAttribute('data-end');
            end = end.split(':');
            if (end.length != 2){
                console.log('--> time-select: wrong data-start attribute element: ', els[i]);
                continue;
            }
            end = Number(end[0]) * 60 + Number(end[1]);

            var interval = Number(els[i].getAttribute('data-interval'));
            var list = document.createElement('div');
            list.setAttribute('class', 'a-items');
            list.innerHTML = '<ul></ul>';
            for (var j = start; j < end; j += interval){
                var option = document.createElement('li');
                option.innerHTML = (Math.floor(j/60)).toString().replace( /^([0-9])$/, '0$1' )+':'+(j%60).toString().replace( /^([0-9])$/, '0$1' );
                option.onclick = function(){
                    this.parentNode.parentNode.previousElementSibling.value = this.innerHTML;
                    this.parentNode.parentNode.previousElementSibling.onchange();
                };
                list.getElementsByTagName('ul')[0].appendChild(option);
            }
            els[i].classList.add('a-selected-item');
            els[i].onclick = function(e){
                if (this.parentNode.classList.contains('opened')){
                    this.parentNode.classList.remove('opened');
                } else {
                    this.parentNode.classList.add('opened');
                }
            };
            els[i].parentNode.appendChild(list);
            els[i].parentNode.classList.add('a-select-wrapper');
            els[i].parentNode.classList.add('time-select-wrapper');
        }

        var els = document.querySelectorAll('.sms-aero select');
        for (var i=0; i < els.length; i++){
            (function(){
                var self = els[i];
                if (self.hasAttribute('data-value') && self.getAttribute('data-value') != "") self.value = self.getAttribute('data-value');
                var options = self.getElementsByTagName('option');
                if (options.length == 0){
                    return false;
                }
                var select = document.createElement('div');
                if (self.hasAttribute('class')){
                    select.setAttribute('class', self.getAttribute('class') + ' a-select-wrapper');
                } else {
                    select.setAttribute('class', 'a-select-wrapper');
                }
                if (self.hasAttribute('style')){
                    select.setAttribute('style', self.getAttribute('style'));
                }

                select.innerHTML = '<div data-value="'+options[self.selectedIndex].innerHTML+'" class="a-selected-item"></div><div class="a-items"><ul></ul></div>';

                select.getElementsByClassName('a-selected-item')[0].onclick = function(e){
                    if (select.classList.contains('opened')){
                        select.classList.remove('opened');
                    } else {
                        select.classList.add('opened');
                    }
                };

                for (var j=0; j < options.length; j++){
                    var option = document.createElement('li');
                    option.innerHTML = options[j].innerHTML;
                    option.setAttribute('data-value', (options[j].hasAttribute('value') ? options[j].getAttribute('value') : options[j].innerHTML));
                    option.onclick = function(){
                        select.getElementsByClassName('a-selected-item')[0].setAttribute('data-value', this.innerHTML);
                        self.value = this.getAttribute('data-value');
                        try {
                            self.onchange();
                        } catch (err) {}
                        try {
                            jQuery(self).trigger('change');
                        } catch (err) {}
                    };
                    select.getElementsByTagName('ul')[0].appendChild(option);
                }

                self.parentNode.insertBefore(select, self);
            }());
        }

        jQuery('.notice-page .a-hider .checkbox input[type="checkbox"]').change(function(){
            var tab = $(this).parent().parent().attr('data-tab');
            jQuery('#' + tab + ' .switcher:first-child ul li').eq((this.checked) ? 1 : 0).click();
        });

        jQuery(".notice-righ-side .switcher").each(function(){
            this.onchange = function(){
                var to_shitch = jQuery(this).parents('.notice-righ-side').attr('id');
                document.getElementById(to_shitch+'_ch').checked = (this.value == 1);
            };
            this.onchange();
        });



    }());
    if (jQuery('.mailign-page').length != 0){
        setInterval('recount_deliv_cost(true)', 10000);
        jQuery('.mailign-page .sms-content').trigger('change');
    }
    if (document.getElementById('to_who') != undefined){
        document.getElementById('to_who').onchange = function(){
            jQuery(this).parent().find('.input-wrapper textarea').val('');
            jQuery('.mailign-page .phone-adder .total-contacts').html(0);
            if (this.value == 1){
                recount_deliv_cost();
                jQuery(this).parent().find('.input-wrapper .a-select-wrapper, .input-wrapper .btn').css('display', 'none');
            } else {
                this.value == 2 ? jQuery(".phone-adder .autocomplete").attr("placeholder", "Введите номер телефона") : jQuery(".phone-adder .autocomplete").attr("placeholder", "Введите e-mail пользователя, совершавшего заказ");
                jQuery(this).parent().find('.input-wrapper .a-select-wrapper, .input-wrapper .btn').css('display', 'inline-block');
            }
        };
        document.getElementById('to_who').onchange();
    }
});
window.addEventListener('click', function(e){
    var is_opened = false;
    if (e.target.classList.contains('a-selected-item') && e.target.parentNode.classList.contains('opened')) is_opened = true;
    var els = document.querySelectorAll('.sms-aero .a-select-wrapper');
    for (var i=0; i < els.length; i++){
        els[i].classList.remove('opened');
        els[i].getElementsByClassName('a-items')[0].style.maxHeight = 0;
    }
    if (is_opened) {
        e.target.parentNode.classList.add('opened');
        if (e.clientY > window.innerHeight/2){
            e.target.nextElementSibling.classList.add('to-top');
            e.target.nextElementSibling.style.maxHeight = (e.clientY - e.target.clientHeight) + 'px';
        } else {
            e.target.nextElementSibling.classList.remove('to-top');
            e.target.nextElementSibling.style.maxHeight = (window.innerHeight - e.clientY - e.target.clientHeight) + 'px';
        }
    }
});

function aero_change_content(startpage, changestate) {
    var rect = document.getElementById("aero_pagination").getBoundingClientRect();
    jQuery.ajax({
        url: ajaxurl,
        type: 'post',
        data: { action: 'smsaero_ajax_stats', startpage: startpage, security:smsaero.nonce },
        success: function(stats) {
            jQuery("#aero_content").html(stats);
            window.scroll(0, jQuery("#aero_pagination").offset().top - rect.top)
            if (changestate) {
                var state = {page: startpage}, title = "Статистика SMSAero, страница "+startpage, newurl = "admin.php?page=smsaero&menu=stats&stat_page="+startpage;
                history.pushState(state, title, newurl);
            }
        }
    });
}

jQuery(document).on('click', '.aero_pagination a', function(event){
    event.preventDefault();
    var startpage = jQuery(this).attr("data-page-counter");
    aero_change_content(startpage, true);
});

window.onpopstate = function(event){
    if(event.state){
        aero_change_content(event.state.page, false);
    }
};