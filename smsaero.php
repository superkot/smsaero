<?php
/*
Plugin Name: SMSAero
Plugin URI: https://smsaero.ru
Description: SMS Aero service for Woocommerce
Version: 1.2
Author URI: https://smsaero.ru
*/
if (!class_exists('smsaero_wc'))
    return;

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
    add_action('plugins_loaded', 'smsaero_wc', 0);

function smsaero_wc()
{
    $smsaero_wc = new smsaero_wc;
}

register_activation_hook( __FILE__, array( 'smsaero_wc', 'smsaero_activation' ) );

/**
 * Получаем все статусы.
 *
 * @version 1.0.6
 * @since   1.0.0
 */
function smsaero_get_order_statuses()
{
    $result = array();
    $statuses = function_exists('wc_get_order_statuses') ? wc_get_order_statuses() : array();
    foreach ($statuses as $status => $status_name) {
        $result[substr($status, 3)] = $status_name;
    }
    return $result;
}

class smsaero_wc
{

    public $smsaero_api;
    public $auth = false;
    public $balance = 0;

    public $login = '';
    public $password = '';

    //Вывод результатов в статистике
    public $count = 10;
    //Страница
    public $page = 0;

    //Не трогать
    public $maxpage = 0;

    function __construct()
    {
        add_action('admin_menu', array(&$this, 'smsaero_menu'));

        add_action('woocommerce_checkout_update_order_meta', array(&$this, 'smsaero_new_order'), 10, 1);
        add_action('woocommerce_checkout_update_user_meta', array(&$this, 'smsaero_new_user'), 11, 2);
        add_action('woocommerce_order_status_changed', array(&$this, 'smsaero_new_status'), 12, 3);

        add_action('wp_ajax_smsaero_get_phones', array(&$this, 'smsaero_get_phones'));
        add_action('wp_ajax_smsaero_ajax_stats', array(&$this, 'smsaero_ajax_stats'));

        add_filter('admin_footer_text', array(&$this, 'disable_footer'));

        include_once('api.php');


        if (isset($_POST['login']) && isset($_POST['password'])) {
            $check_password = new SmsaeroApi($_POST['login'], $_POST['password']);

            $balanceArr = $check_password->balance();
            $balance = $balanceArr['response']['balance'];

            if (isset($balance)) {
                update_option('aero_login', $_POST['login']);
                update_option('aero_psw', $_POST['password']);
                $result = 'Настройки обновлены.';
            } else $result = "Неправильный логин или пароль.";
        }

        //Получаем дефолтные данные
        $this->login = get_option('aero_login');
        $this->password = get_option('aero_psw');

        //settings.php

        if (!empty($this->login) && !empty($this->password)) {
            $this->smsaero_api = new SmsaeroApi($this->login, $this->password);
            $sign = get_option('aero_settings_sign');
            if (strlen($sign) > 1) $this->smsaero_api->set_sign($sign);
            $balance_request = $this->smsaero_api->balance()['response'];
            $d = $this->smsaero_api->checktarif()['response']['reason'];

            if(isset( $d['INTERNATIONAL']))
                update_option('aero_international_channel', $d['INTERNATIONAL']);
            if(isset( $d['INFO']))
                update_option('aero_info_channel', $d['INFO']);
            if(isset( $d['DIRECT']))
                update_option('aero_direct_channel', $d['DIRECT']);
            if(isset( $d['DIGITAL']))
                update_option('aero_digital_channel', $d['DIGITAL']);
                
            $price = $this->smsaero_api->getTariffs('id');
            $getListChanelFull = $this->smsaero_api->getListChanelFull();
            $getListOperators = $this->smsaero_api->getListOperators();
            update_option('aero_channel_price', $price);
            update_option('aero_operator_list', $getListOperators);
            update_option('aero_channel_list_full', $getListChanelFull, true);
            //Проверяем авторизован ли пользователь
            $this->auth = isset($balance_request['balance']);
            if ($this->auth)
                $this->balance = $balance_request['balance'];
        } else $this->auth = false;
    }

    static function smsaero_activation()
    {
        global $wpdb, $charset_collate;
        $smsaero_table = $wpdb->prefix . "smsaero";
        if($wpdb->get_var("SHOW TABLES LIKE '$smsaero_table'") != $smsaero_table ) {
	    	$sql = "CREATE TABLE $smsaero_table (ID BIGINT(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
                   smsaero_id BIGINT(20) unsigned,
                   phone VARCHAR(50),
                   channel VARCHAR(50),
                   sign VARCHAR(255),
                   text VARCHAR(4000),
                   date BIGINT(20) unsigned,
                   price FLOAT unsigned,
                   order_id BIGINT(20) unsigned
                   ) $charset_collate;";
				require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
				dbDelta($sql);
		}
    }

    function smsaero_menu()
    {
        add_submenu_page('woocommerce', 'SMSAero', 'SMS оповещения', 'manage_woocommerce', 'smsaero', array(&$this, 'index'));
    }

    function disable_footer()
    {

    }

    function index()
    {
        global $wpdb;
        //Инициализация css / js
        wp_enqueue_script('smsaero_admin_script', plugins_url('js/sms-aero-ui-kit.js', __FILE__));
        wp_enqueue_script('jquery-plugins', plugins_url('js/jquery.plugins.min.js', __FILE__));
        wp_enqueue_script('jquery-mask', plugins_url('js/jquery.mask.js', __FILE__));
        wp_enqueue_style('smsaero_admin_css', plugins_url('css/default.css', __FILE__));
        
        wp_localize_script('smsaero_admin_script', 'smsaero', array( 'url' => admin_url('admin-ajax.php'), 'nonce' => wp_create_nonce('smsaero-nonce') ) );  

        if (isset($_POST['module_status']))
            if ($_POST['module_status'] == "2") {
                deactivate_plugins(plugin_basename(__FILE__));
                exit(wp_redirect(admin_url('plugins.php')));
            }

        if (isset($_POST['mailing'])) {
            $to = $_POST['to'];
            $send_to = $_POST['send_to'];
            $send_time = $_POST['send_time'];
            $date = $_POST['date'];
            $time = $_POST['time'];
            $text = $_POST['text'];
            update_option('aero_settings_channel', $_POST['settings_channel']);

            $channel = get_option('aero_settings_channel');
            $sign = $_POST['name'];

            //Высчитываем стоимость
            $phones = array();

            if ($to == "1") {
                $res = $wpdb->get_results("SELECT meta_value FROM wp_usermeta WHERE meta_key = 'billing_phone'");
                foreach ($res as $value)
                    $phones[] = $value->meta_value;
            } else
                $phones = explode(PHP_EOL, $send_to);

            $result_ans = false;

            foreach ($phones as $phone) {
                if (!empty(trim($phone)) && mb_strlen(trim($phone) > 7)) {
                    $cost = $this->getCostSMS($text);

                    $phone = $this->clear_phone($phone);

                    $x = $this->smsaero_api->send($phone, $text, ($send_time == "0" ? false : strtotime($date . ' ' . $time)), (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));
                    if ($x['response']['result'] != "reject" && $x['response']['result'] != "no credits") {
                        $o = new MessageStatus($phone, get_option('aero_settings_channel'), $sign,
                            $text, ($send_time == "0" ? time() : strtotime($date . ' ' . $time)), $cost, $x['response']['id']);

                        $this->saveStats($o);
                        $result_ans = true;
                    }
                    //else $result_ans = false;
                }
            }
            $sucs_mailing = '<script> document.addEventListener("DOMContentLoaded", function (){ show_notice(' . ($result_ans ? '"Рассылка успешно отправлена", true' : '"Не удалось создать рассылку", false') . '); }); </script>';
        }

        if (isset($_POST)) {
            foreach ($_POST as $key => $value)
                if ($key != "login" && $key != "password")
                    update_option('aero_' . $key, $value);

            if (count($_POST) > 0)
                $result = 'Данные успешно обновлены.';
        }

        //Получаем страницу
        $this->page = isset($_GET['stat_page']) ? intval($_GET['stat_page']) : '1';

        //Определяем страницу для отображения
        $menu = isset($_GET['menu']) ? $_GET['menu'] : '';

        if (!$this->auth) {
            $result = "Авторизуйтесь!";
            $menu = "settings";
        }

        ?>
        <div class="sms-aero">
        <div class="wrap_logo"><img id="logo_aero" src="<?php echo plugins_url('img/logo_aero.png', __FILE__); ?>"/>
            <div id="notice-side"></div>
        </div>
        <div id="wrapper_left_aero" class='wrap woocommerce'>
        <div id="menu_switcher"></div>
        <ul id="menu_aero">
            <li><a class="btn btn-white-red<?php print ($menu == "mailing" ? " active" : "") ?>"
                   href='<?php echo get_admin_url(null, 'admin.php?page=smsaero&menu=mailing'); ?>'>Массовые
                    рассылки</a></li>
            <li><a class="btn btn-white-red<?php print ($menu == "notice" ? " active" : "") ?>"
                   href='<?php echo get_admin_url(null, 'admin.php?page=smsaero&menu=notice'); ?>'>Уведомления по
                    событиям</a></li>
            <li><a class="btn btn-white-red<?php print ($menu == "stats" ? " active" : "") ?>"
                   href='<?php echo get_admin_url(null, 'admin.php?page=smsaero&menu=stats'); ?>'>Статистика</a></li>
            <li><a class="btn btn-white-red<?php print (($menu == "settings" || empty($menu)) ? " active" : "") ?>"
                   href='<?php echo get_admin_url(null, 'admin.php?page=smsaero&menu=settings'); ?>'>Настройки</a></li>
            <li><a class="btn btn-white-red<?php print ($menu == "help" ? " active" : "") ?>"
                   href='<?php echo get_admin_url(null, 'admin.php?page=smsaero&menu=help'); ?>'>Помощь</a></li>
            <li id="add_balance"><a class="btn btn-green"
                                    href='<?php echo esc_url('https://smsaero.ru/cabinet/sendings/#fullfill-balance'); ?>'>Пополнить
                    баланс</a></li>
        </ul>
        <?php
        if ($this->auth) {
            $senders = $this->smsaero_api->senders()["response"];

            ?>
            <div id="aero_info_block">
                <p id="balance">Баланс: <?php echo $this->balance; ?> <span>&#8381;</span></p>
                <p>Модуль SMSAero</p>
                <p>Версия 1.2</p>
            </div>
            <ul id="aero_soc_block">
                <li><a target="_blank" href="https://vk.com/smsblog"></a>
                <li><a target="_blank" href="https://www.facebook.com/smsaero"></a>
                <li><a target="_blank" href="https://www.twitter.com/smsaero_ru"></a>
            </ul>
            </div>
            <?php
        } else {
            ?>
            </div>
            <?php
        }

        switch ($menu) // переключающее выражение
        {
            case "settings":
                include('templates/settings.php');
                break;
            case "help":
                include('templates/help.php');
                break;
            case "stats":
                include('templates/stats.php');
                break;
            case "notice":
                include('templates/notice.php');
                break;
            case "mailing":
                include('templates/mailing.php');
                break;
            default:
                include('templates/settings.php');
        }
        ?>

        <?php
    }

    /* УВЕДОМЛЕНИЯ ПО СОБЫТИЯМ */

    function smsaero_new_order($order_id)
    {
        //Произошел новый заказ
        $order = new WC_Order($order_id);

        $search = array('{SiteName}', '{OrderID}', '{CartTotal}', '{FirstName}', '{LastName}',
            '{ShippingAddress}', '{ShippingMethod}', '{PaymentAddress}', '{PaymentMethod}', '{BillingPhone}');

        $replace = array(get_bloginfo('name'), $order->id, $order->get_total(), $order->billing_first_name, $order->billing_last_name,
            $order->shipping_address_1 . ' ' . $order->shipping_address_2, $order->get_shipping_method(),
            $order->billing_address_1 . ' ' . $order->billing_address_2, $order->payment_method_title, $order->billing_phone);

        $channel = get_option('aero_settings_channel');
        $sign = get_option('aero_settings_sign');

        if (get_option('aero_notice_new_order_enabled') == "1" && strlen(trim(get_option('aero_notice_new_order_message'))) > 1) {
            if (!empty(trim($order->billing_phone))) {
                $msg = str_replace($search, $replace, get_option('aero_notice_new_order_message'));

                //Высчитываем стоимость
                $cost = $this->getCostSMS($msg);

                $phone = $this->clear_phone($phone);

                //Отправляем SMS
                $x = $this->smsaero_api->send($phone, $msg, false, (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));

                //Создаём объект
                $o = new MessageStatus($order->billing_phone, get_option('aero_settings_channel'), get_option('aero_settings_sign'),
                    $msg, time(), $cost, $x['response']['id']);

                //Сохраняем в статистику
                $this->saveStats($o, $order_id);
            }
        }

        //Уведомление для администраторов
        if (get_option('aero_notice_admin_new_order_enabled') == "1" && strlen(trim(get_option('aero_notice_admin_new_order_message'))) > 1) {
            $phones = explode(PHP_EOL, get_option('aero_notice_admin_new_order_phones'));
            foreach ($phones as $phone) {
                if (!empty(trim($phone))) {
                    $msg = str_replace($search, $replace, get_option('aero_notice_admin_new_order_message'));

                    //Высчитываем стоимость
                    $cost = $this->getCostSMS($msg);

                    $phone = $this->clear_phone($phone);

                    //Отправляем SMS
                    $x = $this->smsaero_api->send($phone, $msg, false, (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));

                    //Создаём объект
                    $o = new MessageStatus($phone, get_option('aero_settings_channel'), get_option('aero_settings_sign'),
                        $msg, time(), $cost, $x['response']['id']);

                    //Сохраняем в статистику
                    $this->saveStats($o, $order_id);
                }
            }
        }
    }

    function smsaero_new_user($user_id, $fields)
    {
        //Новая регистрация
        $search = array('{SiteName}', '{FirstName}', '{LastName}');
        $replace = array(get_bloginfo('name'), $fields['billing_first_name'], $fields['billing_last_name']);

        $channel = get_option('aero_settings_channel');
        $sign = get_option('aero_settings_sign');

        if ($fields['createaccount'] == true) {
            if (get_option('aero_notice_new_signup_enabled') == "1" && strlen(trim(get_option('aero_notice_new_signup_message'))) > 1) {
                if (!empty(trim($fields['billing_phone']))) {
                    $msg = str_replace($search, $replace, get_option('aero_notice_new_signup_message'));

                    //Высчитываем стоимость
                    $cost = $this->getCostSMS($msg);

                    $phone = $this->clear_phone($fields['billing_phone']);

                    //Отправляем SMS
                    $x = $this->smsaero_api->send($phone, $msg, false, (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));

                    //Создаём объект
                    $o = new MessageStatus($fields['billing_phone'], get_option('aero_settings_channel'), get_option('aero_settings_sign'),
                        $msg, time(), $cost, $x['response']['id']);

                    //Сохраняем в статистику
                    $this->saveStats($o);
                }
            }

            if (get_option('aero_notice_admin_new_signup_enabled') == "1" && strlen(trim(get_option('aero_notice_admin_new_signup_message'))) > 1) {
                $phones = explode(PHP_EOL, get_option('aero_notice_admin_new_signup_phones'));
                foreach ($phones as $phone) {
                    if (!empty(trim($phone))) {
                        $msg = str_replace($search, $replace, get_option('aero_notice_admin_new_signup_message'));

                        //Высчитываем стоимость
                        $cost = $this->getCostSMS($msg);

                        $phone = $this->clear_phone($phone);

                        //Отправляем SMS
                        $x = $this->smsaero_api->send($phone, $msg, false, (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));

                        //Создаём объект
                        $o = new MessageStatus($phone, get_option('aero_settings_channel'), get_option('aero_settings_sign'),
                            $msg, time(), $cost, $x['response']['id']);

                        //Сохраняем в статистику
                        $this->saveStats($o);
                    }
                }
            }
        }
    }

    function smsaero_new_status($order_id, $old_status, $new_status)
    {
        //Изменение статуса
        if (get_option('aero_notice_new_status_is_enabled') == "1") {
            $order = new WC_Order($order_id);
            $phone = $order->billing_phone;
            $billing_first_name = get_user_meta($order->get_user_id(), 'billing_first_name', true);
            $billing_last_name = get_user_meta($order->get_user_id(), 'billing_last_name', true);


            $statuses = smsaero_get_order_statuses();
            $status_eng = array_keys($statuses);
            $status_rus = array_values($statuses);

            $new_status_rus = str_replace($status_eng, $status_rus, $new_status);
            $old_status_rus = str_replace($status_eng, $status_rus, $old_status);

            $search = array('{SiteName}', '{OrderID}', '{StatusFrom}',
                '{StatusTo}', '{FirstName}', '{LastName}', '{BillingPhone}');

            $replace = array(get_bloginfo('name'), $order->id, $old_status_rus, $new_status_rus, $billing_first_name, $billing_last_name, $phone);

            foreach ($status_eng as $key) {
                if (get_option('aero_notice_new_status_' . $key) == "1" && strlen(trim(get_option('aero_notice_new_status_message_' . $key))) > 1) {
                    if ($key == $new_status) {
                        if (!empty(trim($phone))) {
                            $msg = str_replace($search, $replace, get_option('aero_notice_new_status_message_' . $key));

                            //Высчитываем стоимость
                            $cost = $this->getCostSMS($msg);

                            //Отправляем SMS

                            $channel = get_option('aero_settings_channel');
                            $sign = get_option('aero_settings_sign');

                            $phone = $this->clear_phone($phone);

                            $x = $this->smsaero_api->send($phone, $msg, false, (strpos($channel, "digital") !== false ? explode("=", $channel)[1] : 0), (strpos($channel, "type") !== false ? explode("=", $channel)[1] : 0));

                            //Создаём объект
                            $o = new MessageStatus($phone, $channel, $sign,
                                $msg, time(), $cost, $x['response']['id']);

                            //Сохраняем в статистику
                            $this->saveStats($o, $order_id);
                        }
                    }
                }
            }
        }
    }

    function isRussian($text)
    {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }

    function getTextFromType($str)
    {
        $types = array("type=2" => 'Бесплатная буквенная подпись для всех операторов, кроме МТС.',
            "type=3" => 'Бесплатная буквенная подпись для всех операторов (+0,15 рублей к тарифу Прямого канала).',
            "type=4" => 'Инфоподпись для всех операторов.',
            "type=6" => 'Международная доставка (Операторы РФ и Казахстана).',
            "type=7" => 'Рекламный канал.',
            "type=8" => 'Сервисный канал.',
            "digital=1" => 'Цифровой канал отправки (допускается только транзакционный трафик).');

        return $types[$str];
    }

    function getCostSMS($msg, $numbers = 1)
    {
        $channel = get_option('aero_settings_channel');
        $length = mb_strlen($msg);
        $message_length = 160;

        $price = floatval(get_option('aero_direct_channel'));

        switch ($channel) {
            case "type=4":
                $price = floatval(get_option('aero_info_channel'));
                break;
            case "type=6":
                $price = floatval(get_option('aero_international_channel'));
                break;
            case "type=3":
                $price = floatval(get_option('aero_direct_channel')) + 0.15;
                break;
            case "digital=1":
                $price = floatval(get_option('aero_digital_channel'));
                break;
            default:
                $price = floatval(get_option('aero_direct_channel'));
        }

        if ($this->isRussian($msg)) ;
        $message_length = 70;

        $total_sms = ceil($length / $message_length) * $numbers;
        $total_sum = $total_sms * $price;

        return round($total_sum, 2);
    }

    function saveStats($o, $order_id = 0)
	{
        global $wpdb;
        $smsaero_table = $wpdb->prefix . "smsaero";
        $q = "INSERT INTO $smsaero_table ( smsaero_id, phone, channel, sign, text, date, price, order_id ) VALUES ('%d', '%s', '%s', '%s', '%s', '%d', '%f', '%d' )" ;
        $wpdb->query($wpdb->prepare($q, $o->id, $o->phone, $o->channel, $o->sign, $o->text, $o->date, $o->price, $order_id));
    }

    function clear_phone($phone)
    {
        $phone = str_replace("+7", "8", $phone);
        $phone = preg_replace('/[^0-9]/', '', $phone);
        return $phone;
    }

    function smsaero_get_phones()
    {
        check_ajax_referer( 'smsaero-nonce', 'security' );
        
        $query = $_POST['query'];
        global $wpdb;
    
        if('full' == $query) {
            $res = $wpdb->get_results( "SELECT id FROM wp_users" );
        }
        elseif ('email' == $query && isset($_POST['email']) && !empty($_POST['email']) ) {
            $q = $_POST['email'];
        	$res = $wpdb->get_results( $wpdb->prepare("SELECT id FROM wp_users WHERE user_email LIKE '%s'", '%'.$q.'%') );
        }
        else {
            wp_die();
        }
    
        foreach ($res as $value){
            $get_phone_sql = $wpdb->get_results( $wpdb->prepare("SELECT meta_value FROM wp_usermeta WHERE user_id = '%d' AND meta_key = 'billing_phone'", $value->id) );
            if(count($get_phone_sql) > 0)
            {
                $resp[] = $this->clear_phone($get_phone_sql[0] -> meta_value);
            }
    	}
     
    	if('email' == $query) {
    	    $resp = array_slice($resp,0,10);
    	}
    	
    	echo json_encode($resp); 
    	wp_die();
    }	
	
    function smsaero_ajax_stats()
    {
        check_ajax_referer( 'smsaero-nonce', 'security' );
        $this->smsaero_show_stats($_POST['startpage']);
        wp_die();
    }

    function smsaero_show_stats($startpage = 1) {
        global $wpdb;
        $smsaero_table = $wpdb->prefix . "smsaero";
        $count_query = "SELECT COUNT(*) FROM $smsaero_table";
        $total_messages = $wpdb->get_var($count_query);
        
        if($total_messages)
        {
            $this->maxpage = ceil($total_messages / $this->count);
            $startpage = ($startpage <= $this->maxpage ? $startpage : $this->maxpage);
            $startpage = ($startpage >= 1 ? $startpage : 1);
            $limit_start = ($startpage - 1) * $this->count;
            $statistic = $wpdb->get_results( "SELECT * FROM {$smsaero_table} ORDER BY ID DESC LIMIT $limit_start, $this->count" );
        
            echo '<table class="table_stats">
            <thead>
            <tr>
            <th class="align_left">Телефон</th>
            <th class="align_left">Канал отправки</th>
            <th class="align_left">Подпись</th>
            <th class="align_left">Текст сообщения</th>
            <th class="align_left">Дата отправки</th>
            <th class="align_left">Стоимость</th>
            </tr>
            </thead>
            <tbody>';
        
            if(!empty($statistic))
            {
                foreach($statistic as $st) 
                {
                    print '
                    <tr>
                    <td>'.$st->phone.'</td>
                    <td>'.$this->getTextFromType($st->channel).'</td>
                    <td style="color: #5d6978;">'.$st->sign.'</td>
                    <td>'.$st->text.'</td>
                    <td>'.$this->rus_date("j F Y H:i ", $st->date).'</td>
                    <td>'.$st->price.' <span>&#8381;</span></td>
                    </tr>';
                }
            }
    
            echo '</tbody></table>
            <div class="aero_pagination_wrapper">';

            if($this->maxpage > 1)
            {
                echo '<ul class="aero_pagination" id="aero_pagination">
                    '.($startpage > 2 ? '<li class="start"><a class="btn btn-blue" data-page-counter="1" href="#" title="В начало" rel=""><span class="fa fa-angle-double-left"></span></a></li>' : '').
                    (($startpage - 1) >= 1 ? '<li><a class="btn btn-white-red" data-page-counter="'.($startpage - 1).'" href="#">'.($startpage - 1).'</a></li>' : '').'
                    <li class="active btn btn-white-red">'.$startpage.'</li>
                    '.(($startpage + 1) <= $this->maxpage ? '<li><a class="btn btn-white-red" data-page-counter="'.($startpage + 1).'" href="#">'.($startpage + 1).'</a></li>' : '')
                    .(($this->maxpage - $startpage) > 1 ? '<li class="end"><a class="btn btn-blue" title="В конец" data-page-counter="'.$this->maxpage.'" href="#" rel=""><span class="fa fa-angle-double-right"></span></a></li>' : '').'
                    </ul>';
            }
            echo '</div>';
        }
        
        else
        {
            echo '<h3>Ваша статистика пока пуста.</h3>';
        }
        
        echo '</div>';
        
    }

    function rus_date()
    {
        $translate = array(
			 "am" => "дп",
			 "pm" => "пп",
			 "AM" => "ДП",
			 "PM" => "ПП",
			 "Monday" => "Понедельник",
			 "Mon" => "Пн",
			 "Tuesday" => "Вторник",
			 "Tue" => "Вт",
			 "Wednesday" => "Среда",
			 "Wed" => "Ср",
			 "Thursday" => "Четверг",
			 "Thu" => "Чт",
			 "Friday" => "Пятница",
			 "Fri" => "Пт",
			 "Saturday" => "Суббота",
			 "Sat" => "Сб",
			 "Sunday" => "Воскресенье",
			 "Sun" => "Вс",
			 "January" => "Января",
			 "Jan" => "Янв",
			 "February" => "Февраля",
			 "Feb" => "Фев",
			 "March" => "Марта",
			 "Mar" => "Мар",
			 "April" => "Апреля",
			 "Apr" => "Апр",
			 "May" => "Мая",
			 "May" => "Мая",
			 "June" => "Июня",
			 "Jun" => "Июн",
			 "July" => "Июля",
			 "Jul" => "Июл",
			 "August" => "Августа",
			 "Aug" => "Авг",
			 "September" => "Сентября",
			 "Sep" => "Сен",
			 "October" => "Октября",
			 "Oct" => "Окт",
			 "November" => "Ноября",
			 "Nov" => "Ноя",
			 "December" => "Декабря",
			 "Dec" => "Дек",
			 "st" => "ое",
			 "nd" => "ое",
			 "rd" => "е",
			 "th" => "ое"
        );
        if (func_num_args() > 1) {
            $timestamp = func_get_arg(1);
            return strtr(date(func_get_arg(0), $timestamp), $translate);
        } else {
            return strtr(date(func_get_arg(0)), $translate);
        }
    }

}


class MessageStatus
{
    public $id;
    public $phone;
    public $channel;
    public $sign;
    public $text;
    public $date;
    public $price;

    function __construct($_phone, $_channel, $_sign, $_text, $_date, $_price, $_id)
    {
        $this->phone = $_phone;
        $this->channel = $_channel;
        $this->sign = $_sign;
        $this->text = $_text;
        $this->date = $_date;
        $this->price = $_price;
        $this->id = $_id;
    }
}
