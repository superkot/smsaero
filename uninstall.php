<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

delete_option('aero_login');
delete_option('aero_psw');
delete_option('aero_settings_sign');
delete_option('aero_settings_channel');
delete_option('aero_international_channel');
delete_option('aero_info_channel');
delete_option('aero_direct_channel');
delete_option('aero_digital_channel');
delete_option('aero_operator_list');
delete_option('aero_channel_list_full');

global $wpdb;
$smsaero_table = $wpdb->prefix . 'smsaero';
$q = "DROP TABLE IF EXISTS $smsaero_table";
$wpdb->query($q);

?>