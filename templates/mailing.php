<?php
if ( ! defined( 'ABSPATH' ) )
    exit;


$price = get_option('aero_channel_price');
$operatorList = get_option('aero_operator_list');
$operatorList = json_decode($operatorList);
$getListChanelFull = get_option('aero_channel_list_full');
$getListChanelFull = json_decode($getListChanelFull);

?>
<script>
    jQuery(document).ready(function ($) {
        price = <?= $price ?>;
    });
</script>
<div id="aero_content" class="mailign-page">
    <form method='post' name='mailingform' action="<?php echo admin_url('admin.php?page=smsaero&menu=mailing');?>">
        <div class="request__subs">
            <a href="https://smsaero.ru/cabinet/settings/signs/">Запросить другие подписки <i class="fa fa-sign-in"></i></a>
        </div>
        <?php if (isset($sucs_mailing)) { echo '<h3>'.$sucs_mailing.'</h3>'; } ?>
        <div style="float: left; width: 100%;">
            <div class="a-row">
                <div class="a-col-1-2">
                    <small>Выбор имени отправителя</small>
                </div>
                <div class="a-col-1-2">
                    <select name="name" data-value="<?php print get_option('aero_settings_sign'); ?>">
                        <?php foreach($senders as $s) print '<option>'. $s .'</option>'; ?>
                    </select>
                </div>
            </div>
            <div class="a-row">
                <div class="a-col-1-2">
                    <small>Выбор получателей</small>
                    <small><i class="fa fa-info-circle"></i> Выберите клиентов, которым вы хотите отправить</small>
                </div>
                <div class="a-col-1-2">

                    <select id="to_who" name="to">
                        <option value="1">Всем пользователям</option>
                        <option value="2">Произвольные номера</option>
                        <option value="3">Пользователи, совершившие покупку (поиск по заказам)</option>
                    </select>
                    <div class="input-wrapper">
                        <div class="phone-adder">
                            <div>
							<input type="text" class="autocomplete" autocomplete="off" autocorrect="off" spellcheck="false" data-symbols-start="2" />
                            </div>
                            <button type="button" class="btn btn-blue">Добавить</button>
                            <textarea name="send_to" placeholder=""></textarea>
                            <div class="input-info">Итого контактов: <span class="red-text total-contacts">0</span></div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="a-row">
                <div class="a-col-1-2">

                    <small>Время отправки рассылки</small>

                    <small><i class="fa fa-info-circle"></i> Выберите время, когда сообщения будут отправлены</small>
                </div>
                <div class="a-col-1-2 time_sending">
                    <div class="input-wrapper">
                        <label class="radio"><input id="immediate_send" name="send_time" type="radio" value="0" /><span>Немедленно</span></label>
                        <br/><br/>
                        <label class="radio"><input id="delay_send" name="send_time" type="radio" value="1" /><span>В запланированое время</span></label>
                    </div>
                    <div><small>Дата и время начала отправки</small></div>
                    <div class="date-time-select">
                        <div class="input-wrapper">
                            <div class="date-select">
                                <input name="date" type="text" value="<?php echo date('d.m.Y'); ?>" class="js-date-sending" />
                            </div>
                        </div>
                        <div class="input-wrapper">
                            <input name="time" value="00:00" data-start="00:00" data-end="23:50" data-interval="30" class="time-select" />
                        </div>
                    </div>
                    <div><small>Внимание! Указывается московское время.</small></div>
                </div>
            </div>
            <div class="a-row">
                <div class="a-col-1-2">
                    <small>Текст рассылки</small>
                </div>
                <div class="a-col-1-2">
                    <div class="input-wrapper">
                        <textarea class="sms-content check-words" name="text" placeholder=""></textarea>
                        <div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
                    </div>
                </div>
            </div>
            <div class="a-row">
                <div class="a-col-1-2">
                    <small>Выбор канала отправки</small>
                    <small><a href="https://smsaero.ru/blog/post/kak-vybrat-kanal-rassylki-sms-novye-vozmozhnosti-sms-aero/"><i class="fa fa-info-circle"></i> Какой канал выбрать</a></small>
                </div>
                <div class="a-col-1-2">
                    <select name="settings_channel" class="aero_select_style" name="channel" data-value="type=7">
                        <?php foreach ($getListChanelFull as $k => $channel) { ?>
                            <option value="<?= $k == 5 ? 'digital' : 'type' ?>=<?= $k == 5 ? '1' : $k  ?>" data-channel="<?= $k ?>" <?= ($channel == reset($getListChanelFull)) ? 'selected' : '' ?>><?= $channel ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="a-row">
                <div class="a-col-1-2">
                    <div class="box-count-aerosms">
                        <?php foreach($operatorList as $k => $operator) { ?>
                            <div>
                                <small><?= $operator ?>: </small>
                                <span class="operator-count" id="operator-id-<?= $k ?>">0</span> SMS
                            </div>
                        <?php } ?>
                        <div>
                            <small>Итого: </small>
                            <span class="count-aerosmswrapper">0</span> SMS
                        </div>
                    </div>
                    <div class="box-count-aerosms">
                        <small>Стоимость рассылки:</small>
                        <div>
                            <span class="cost-aerosmswrapper">0</span> &#8381;
                        </div>
                    </div>
                </div>
                <div class="a-col-1-2">
                    <button name="mailing" style="margin-top: 5px;" type="submit" class="btn btn-green">Отправить</button>
                </div>
            </div>
    </form>
</div>