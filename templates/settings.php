<?php

if ( ! defined( 'ABSPATH' ) )

    exit;

$channel_opt = get_option('aero_settings_channel');
$getListChanelFull = get_option('aero_channel_list_full');
$getListChanelFull = json_decode($getListChanelFull);

?><div id="aero_content" class="wrapper_aero_setting">

    <form method='post' id='mainform' action="<?php echo admin_url('admin.php?page=smsaero&menu=settings');?>">

        <div class='icon32 icon32-woocommerce-settings' id='icon-woocommerce'></div>

        <?php if (isset($result)) { echo '<h3>'.$result.'</h3>'; } ?>

        <div class="aero_module_setting">

            <h3>Настройки модуля</h3>

            <?php if(isset($senders) && count((array)$senders) > 0)

            {

                ?>

                <div class="group_setting">

                    <label><small>Выбор подписи для уведомлений</small></label>

                    <select name="settings_sign" class="aero_select_style" data-value="<?php print get_option('aero_settings_sign'); ?>">

                        <?php foreach($senders as $s) print '<option>'. $s .'</option>'; ?>

                    </select>

                </div>

            <?php } ?>

            <div class="group_setting">

                <label><small>Выбор канала отправки уведомлений</small></label>

                <select name="settings_channel" class="aero_select_style" data-value="<?= $channel_opt ?>">
                    <?php foreach ($getListChanelFull as $k => $channel) { ?>
                        <option value="<?= $k == 5 ? 'digital' : 'type' ?>=<?= $k == 5 ? '1' : $k  ?>"><?= $channel ?></option>
                    <?php } ?>
                </select>

            </div>

            <div class="group_setting">

                <label><small>Статус модуля</small></label>

                <select class="aero_select_style" name="module_status">

                    <option value="1" selected>Включен.</option>

                    <option value="2">Выключен.</option>

                </select>

            </div>

        </div>

        <div class="aero_connect_setting">

            <h3>Настройки подключения</h3>

            <div class="group_setting input-wrapper">

                <label for='login'><small>Логин</small></label>

                <input class="aero-align-center" type='text' name='login' placeholder='' value='<?php echo $this->login;?>'>

            </div>

            <div class="group_setting input-wrapper">

                <label for='password'><small>Пароль</small></label>

                <input class="aero-align-center" type='password' name='password' placeholder='' value='<?php echo (strlen($this->password) > 0 ? $this->password : "*******");?>'>

            </div>

        </div>

        <div style="margin-top: 80px;"><input type='submit' class="btn btn-green" value='Сохранить'></div>

    </form> <div class="clear"></div></div>
