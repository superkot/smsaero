<?php

	if ( ! defined( 'ABSPATH' ) ){
        exit;
    }

    include_once(dirname(__FILE__).'/../smsaero.php' );

    $statuses = smsaero_get_order_statuses();
?>
<div id="aero_content" class="notice-page">
	<div class="a-row">
		<div class="a-col-1-3">
			<div class="a-hider">
				<input id="notice-sms-client" type="checkbox" checked >
				<label for="notice-sms-client">Отправка SMS клиенту</label>
				<ul>
					<li data-tab="client-new-order" class="input-wrapper active">
						<label class="checkbox"><input id="client-new-order_ch" type="checkbox" /><span></span></label> Новый заказ
					</li>
					<li data-tab="client-good-state-change" class="input-wrapper">
						<label class="checkbox"><input name="notice_new_status_is_enabled" value="1" id="client-good-state-change_ch" type="checkbox" /><span></span></label> Изменение статуса товара
					</li>
					<li data-tab="client-new-reg" class="input-wrapper">
						<label class="checkbox"><input id="client-new-reg_ch" type="checkbox" /><span></span></label> Новая регистрация
					</li>
				</ul>
			</div>
			<div class="a-hider">
				<input id="notice-sms-admin" type="checkbox"  checked>
				<label for="notice-sms-admin">Отправка SMS администратору</label>
				<ul>
					<li data-tab="admin-new-order" class="input-wrapper">
						<label class="checkbox"><input id="admin-new-order_ch" type="checkbox" /><span></span></label> Новый заказ
					</li>
					<li data-tab="admin-new-reg" class="input-wrapper">
						<label class="checkbox"><input id="admin-new-reg_ch" type="checkbox" /><span></span></label> Новая регистрация
					</li>
				</ul>
			</div>
		</div>
		<form id="client-new-order" style="display: block;" class="a-col-2-3 notice-righ-side" method='post' action="<?php echo admin_url('admin.php?page=smsaero&menu=notice');?>">
			<div class="a-row">
				<div class="a-col-1-2">
					<small>Статус</small>
				</div>
				<div class="a-col-1-2">
					<select name="notice_new_order_enabled" data-value="<?php print get_option('aero_notice_new_order_enabled'); ?>" class="switcher">
						<option value="0">Отключено</option>
						<option value="1">Включено</option>
					</select>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<p><small>Сообщение:</small></p>
					<small>Коды для замены:</small>
					<small>{SiteName} - Название магазина</small>
					<small>{OrderID} - ID заказа</small>
					<small>{CartTotal} – Сумма заказа</small>
					<small>{FirstName} - Имя покупателя</small>
					<small>{LastName} – Фамилия покупателя</small>
					<small>{BillingPhone} – Телефон покупателя</small>
					<small>{ShippingAddress} – Адрес доставки</small>
					<small>{ShippingMethod} – Метод доставки</small>
					<small>{PaymentAddress} – Адрес оплаты</small>
					<small>{PaymentMethod} – Метод оплаты</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<textarea name="notice_new_order_message" class="sms-content" placeholder=""/><?php print get_option('aero_notice_new_order_message'); ?></textarea>
						<div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
					</div>
				</div>
			</div>
			<div class="a-row"><button class="btn btn-green">Сохранить</button></div>
		</form>
		<form id="client-good-state-change" class="a-col-2-3 notice-righ-side" method='post' action="<?php echo admin_url('admin.php?page=smsaero&menu=notice');?>">
			<div class="a-row">
				<div class="a-col-1-2">
					<small>Статус</small>
				</div>
				<div class="a-col-1-2">
					<select name="notice_new_status_is_enabled" data-value="<?php print get_option('aero_notice_new_status_is_enabled'); ?>" class="switcher">
						<option value="0">Отключено</option>
						<option value="1">Включено</option>
					</select>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<small>Статус заказа</small>
					<small>Выберите статус заказа. При каждом изменении статуса клиент будет получать уведомления по SMS.</small>
					<br/>
					<small>Коды для замены:</small>
					<small>{SiteName} – Название магазина</small>
					<small>{OrderID} – ID заказа</small>
					<small>{StatusFrom} – Статус изменился с</small>
					<small>{StatusTo} – Статус изменился на </small>
					<small>{FirstName} – Имя покупателя</small>
					<small>{LastName} – Фамилия покупателя</small>
					<small>{BillingPhone} – Телефон покупателя</small>
				</div>
				<div class="a-col-2-3">
					<ul class="order-statuses">

                        <?php foreach ($statuses as $status_eng => $status_rus): ?>
						<li class="input-wrapper">
							<input type='hidden' value='0' name='notice_new_status_<?= $status_eng ?>'>
							<label class="checkbox"><input name="notice_new_status_<?= $status_eng ?>" type="checkbox" value="1" <?php print (get_option('aero_notice_new_status_' . $status_eng) == "1" ? "checked" : ""); ?> /><span><?= $status_rus ?></span></label>
							<label class="input-title">Сообщение</label>
							<textarea name="notice_new_status_message_<?= $status_eng ?>" class="sms-content" placeholder=""/><?php print get_option('aero_notice_new_status_message_' . $status_eng); ?></textarea>
							<div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
						</li>
                        <?php endforeach; ?>

					</ul>
				</div>
			</div>
			<div class="a-row"><button class="btn btn-green">Сохранить</button></div>
		</form>
		<form id="client-new-reg" class="a-col-2-3 notice-righ-side" method='post' action="<?php echo admin_url('admin.php?page=smsaero&menu=notice');?>">
			<div class="a-row">
				<div class="a-col-1-2">
					<small>Статус</small>
				</div>
				<div class="a-col-1-2">
					<select name="notice_new_signup_enabled" data-value="<?php print get_option('aero_notice_new_signup_enabled'); ?>" class="switcher">
						<option value="0">Отключено</option>
						<option value="1">Включено</option>
					</select>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<p><small>Сообщение:</small></p>
					<small>Коды для замены:</small>
					<small>{SiteName} – Название магазина </small>
					<small>{FirstName} – Имя покупателя</small>
					<small>{LastName} – Фамилия покупателя</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<textarea name="notice_new_signup_message" class="sms-content" placeholder=""/><?php print get_option('aero_notice_new_signup_message'); ?></textarea>
						<div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
					</div>
				</div>
			</div>
			<div class="a-row"><button class="btn btn-green">Сохранить</button></div>
		</form>
		<form id="admin-new-order" class="a-col-2-3 notice-righ-side" method='post' action="<?php echo admin_url('admin.php?page=smsaero&menu=notice');?>">
			<div class="a-row">
				<div class="a-col-1-2">
					<small>Статус</small>
				</div>
				<div class="a-col-1-2">
					<select name="notice_admin_new_order_enabled" data-value="<?php print get_option('aero_notice_admin_new_order_enabled'); ?>" class="switcher">
						<option value="0">Отключено</option>
						<option value="1">Включено</option>
					</select>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<p><small>Сообщение:</small></p>
					<small>Коды для замены:</small>
					<small>{SiteName} - Название магазина</small>
					<small>{OrderID} - ID заказа</small>
					<small>{CartTotal} – Сумма заказа</small>
					<small>{FirstName} - Имя покупателя</small>
					<small>{LastName} – Фамилия покупателя</small>
					<small>{BillingPhone} – Телефон покупателя</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<textarea name="notice_admin_new_order_message" class="sms-content" placeholder=""/><?php print get_option('aero_notice_admin_new_order_message'); ?></textarea>
						<div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
					</div>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<small>Телефон администратора</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<div class="phone-adder">
							<textarea placeholder="Каждый номер должен быть с новой строчки." name="notice_admin_new_order_phones"><?php print get_option('aero_notice_admin_new_order_phones'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="a-row"><button class="btn btn-green">Сохранить</button></div>
		</form>
		<form id="admin-new-reg" class="a-col-2-3 notice-righ-side" method='post' action="<?php echo admin_url('admin.php?page=smsaero&menu=notice');?>">
			<div class="a-row">
				<div class="a-col-1-2">
					<small>Статус</small>
				</div>
				<div class="a-col-1-2">
					<select name="notice_admin_new_signup_enabled" data-value="<?php print get_option('aero_notice_admin_new_signup_enabled'); ?>" class="switcher">
						<option value="0">Отключено</option>
						<option value="1">Включено</option>
					</select>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<p><small>Сообщение:</small></p>
					<small>Коды для замены:</small>
					<small>{SiteName} – Название магазина </small>
					<small>{FirstName} – Имя покупателя</small>
					<small>{LastName} – Фамилия покупателя</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<textarea name="notice_admin_new_signup_message" class="sms-content" placeholder=""/><?php print get_option('aero_notice_admin_new_signup_message'); ?></textarea>
						<div class="input-info">Символов <span class="red-text"><span class="total-symbols">0</span>/<span class="symbols-to-sms">160</span></span>&nbsp;&nbsp;&nbsp;SMS <span class="sms-count red-text">0</span></div>
					</div>
				</div>
			</div>
			<div class="a-row">
				<div class="a-col-1-3">
					<small>Телефон администратора</small>
				</div>
				<div class="a-col-2-3">
					<div class="input-wrapper">
						<div class="phone-adder">
							<textarea placeholder="Каждый номер должен быть с новой строчки." name="notice_admin_new_signup_phones"><?php print get_option('aero_notice_admin_new_signup_phones'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="a-row"><button class="btn btn-green">Сохранить</button></div>
		</form>
	</div>
</div>