<?php
	if ( ! defined( 'ABSPATH' ) )
		exit;
		
echo '<script>
var page = {page: '.$this->page.'};
history.pushState(page, "Статистика SMSAero, страница '.$this->page.'", "admin.php?page=smsaero&menu=stats&stat_page='.$this->page.'");
</script>
<div id="aero_content" class="wrapper_aero_stats">';

unset($_GET['stat_page']);

$this->smsaero_show_stats($this->page);