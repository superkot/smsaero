<?php
	if ( ! defined( 'ABSPATH' ) )
		exit;
?>
<div id="aero_content" class="wrapper_aero_help">
	<div class="item_help_box aero_ticket">
		<h5>Техническая поддержка</h5>
		<div class="a-row">
			<div class="a-col-1-2">
				<p>Бесплатно по России</p>
				<p><a class="phone" href="tel:+78005557550" title="Контактный телефон">8 800 555 7 550</a></p>
			</div>
			<div class="a-col-1-2">
				<p><a href="mailto:support@smsaero.ru?Subject=Hello!" title="Электронная почта SMS Aero"><i class="fa fa-envelope"></i>support@smsaero.ru</a></p>
				<p><a href="skype:smsaero" title="Skype SMS Aero"><i class="fa fa-skype"></i>smsaero</a></p>
			</div>
		</div>
	</div>	
	<div class="item_help_box aero_cooperate">	
		<h5>Сотрудничество</h5>	
		<p>
			Есть предложения по партнерству или развитию модуля?
			Мы рассмотрим все идеи и обращения</p>	
		<a href="mailto:support@smsaero.ru" class="btn btn-white-red">Написать</a>	
	</div>
</div>